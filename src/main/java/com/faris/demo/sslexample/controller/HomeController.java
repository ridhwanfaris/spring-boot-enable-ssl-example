package com.faris.demo.sslexample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/home")
public class HomeController {

    @GetMapping("/message")
    public String getMessage(){
        return "Welcome to Spring Boot SSL Example";
    }
}
